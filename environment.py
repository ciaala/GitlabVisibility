import os
from typing import Optional

from dotenv import load_dotenv


class Environment:
    def __init__(self,
                 property_file: Optional[str] = None):
        self.property_file = property_file
        if len(self.property_file) != 0:
            if os.path.exists(self.property_file):
                load_dotenv(self.property_file)
            else:
                raise ValueError(f'Environment file {self.property_file} does not exist')

    def required(self,
                 env_property: str) -> str:
        value = os.environ.get(env_property)
        if value is None or len(value) == 0:
            raise ValueError(f'Environment variable {env_property} is not set')
        return value

    def get(self,
            env_property: str,
            default_value: str = None) -> Optional[str]:
        return os.environ.get(env_property)
