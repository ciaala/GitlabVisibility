import csv
import logging

import requests


def update_project_visibility(filename: str,
                              access_token: str) -> None:
    gitlab_api = 'https://gitlab.com/api/v4'
    headers = {
        'Authorization': f'Bearer {access_token}'}
    with open(filename,
              'r') as file:
        reader = csv.DictReader(file)
        for row in reader:
            project_id = row['Project ID']
            project_name = row['Project Name']
            new_visibility = row['Visibility']
            response = requests.put(f'{gitlab_api}/projects/{project_id}',
                                    headers=headers,
                                    data={
                                        'visibility': new_visibility})
            if response.status_code == 200:
                logging.info(f"Project '{project_name}' Success. Visibility changed to {new_visibility}")
            else:
                logging.error(f"Project '{[project_name]}' Failure. Could not to change visibility to {new_visibility}")
                logging.error(f"Response: {response.text[0:64]}")
