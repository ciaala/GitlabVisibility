import logging
import os
from pathlib import Path

from environment import Environment
from list import list_projects
from update_visibility import update_project_visibility




if __name__ == '__main__':
    env = Environment("properties.env")
    access_token = env.required('access_token')
    filename = env.get('projects_filename', 'project_visibility_mapping.csv')
    logging.basicConfig(format='%(asctime)s:%(filename)s:%(levelname)s:%(message)s')

    logging.info(f'Update Project Visibility: filename: {filename}')
    path = Path(filename)
    if not path.exists():
        list_projects(filename,
                      access_token)
    elif path.is_file():
        update_project_visibility(filename,
                                  access_token)
    else:
        raise FileExistsError('File exists, but is not a file')
