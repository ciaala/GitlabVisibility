import csv
import requests

# Replace 'your_access_token' with your actual GitLab personal access token




def list_projects(output_filename: str, access_token: str) -> None:
    gitlab_api = 'https://gitlab.com/api/v4'
    headers = {
        'Authorization': f'Bearer {access_token}'}
    projects = []
    page = 1
    while True:
        response = requests.get(f'{gitlab_api}/projects?membership=true&per_page=100&page={page}',
                                headers=headers)
        if response.status_code != 200:
            break
        data = response.json()
        if not data:
            break
        projects.extend(data)
        page += 1

    with open(output_filename,
              'w',
              newline='') as file:
        writer = csv.writer(file)
        writer.writerow(['Project Name', 'Visibility', 'Project ID'])
        for project in projects:
            writer.writerow([project['name'], project['visibility'], project['id']])

    print(f'{len(projects)} projects have been written to project_visibility_mapping.csv')


